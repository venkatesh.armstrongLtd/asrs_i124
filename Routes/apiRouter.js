const router = require('express').Router()
const {insertRecords, getAllRecords, getRecordById, deletRecordById, updateRecord} = require('../controller/crudHandler.js')

router.post('/insert', insertRecords)
router.get('/allrecords', getAllRecords)
router.get('/getrecord/:UOMId', getRecordById )
router.delete('/removerecord/:UOMId', deletRecordById)
router.patch('/updaterecord/:UOMId', updateRecord)

module.exports = router