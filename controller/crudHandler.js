const mssql = require('mssql')
const sqlConfig = require('../db/dbconfig')

const queryStatus = {
    0: "Successful execution",                        //0-4 default return values by ms sql server
    1: "Required parameter value is not specified",
    2: "Specified parameter value is not valid",
    3: "Error has occurred getting column value",
    4: "NULL column value found for the table",
    5: "No access for Duplicate entry, please check input values"  //added externally to avoid duplicate values.
}

const insertRecords = async(req, res)=> {
    const data = {...req.body}
    //data validation required
    try {
        let pool = await mssql.connect(sqlConfig)
        let insertData = await pool.request()
            .input('UOM', mssql.NVarChar, data.UOM)
            .input('isActive', mssql.Int, data.isActive)
            .input('LastModifiedOn', mssql.DateTime, data.LastModifiedOn)
            .input('LastModifiedBy', mssql.NVarChar, data.LastModifiedBy)
            .execute('insertRecords')
            console.dir(insertData)
            if (insertData.returnValue === 0) return res.status(200).json({'status': queryStatus[insertData.returnValue]})
            return res.status(404).json({'status': queryStatus[insertData.returnValue]})
      }
      catch (err) {
        console.log(err);
        return res.status(500).json({'status': err.message})
      }
}

const getAllRecords = async(req, res)=> {
    try {
        let pool = await mssql.connect(sqlConfig)
        let getAllData = await pool.request()
            .execute('getAllRecords')
            console.log('-----', getAllData)
            if (getAllData.returnValue === 0) return res.status(200).json({
                'status': queryStatus[getAllData.returnValue],
                'data': getAllData.recordset
        })
            return res.status(404).json({'status': queryStatus[getAllData.returnValue]})
      }
      catch (err) {
        console.log(err);
        return res.status(500).json({'status': err.message})
      }
}

const getRecordById = async(req, res)=> {
    const { UOMId }= req.params
    try{
        let pool = await mssql.connect(sqlConfig)
        let getRecord = await pool.request()
                    .input('UOMId', mssql.Int, UOMId)
                    .execute('getRecordbyId')
            console.log('---', getRecord)
        if (getRecord.returnValue === 0 && getRecord.recordset.length > 0) return res.status(200).json({
                'status': queryStatus[getRecord.returnValue],
                'data': getRecord.recordset[0]
            })
        if (getRecord.returnValue === 0 && getRecord.recordset.length === 0) return res.status(404).json({
            'status': `No data found for this id : ${UOMId}`
        })
        return res.status(404).json({'status': queryStatus[getRecord.returnValue]})
    }catch (err) {
        console.log(err);
        return res.status(500).json({'status': err.message})
      }
}

const deletRecordById = async(req, res)=> {
    const { UOMId }= req.params
    try{
        let pool = await mssql.connect(sqlConfig)
        let getRecord = await pool.request()
                    .input('UOMId', mssql.Int, UOMId)
                    .execute('deleteRecordbyId')
            console.log('---', getRecord)
        if (getRecord.returnValue === 0) return res.status(200).json({
                'status': 'Record is successfully deleted'
            })
        return res.status(404).json({'status': queryStatus[getRecord.returnValue]})
    }catch (err) {
        console.log(err);
        return res.status(500).json({'status': err.message})
      }
}

const updateRecord = async(req, res)=> {
    const { UOMId }= req.params
    const data = {...req.body}
    try{
        let pool = await mssql.connect(sqlConfig)
        let updateRecord = await pool.request()
                    .input('UOMId', mssql.Int, UOMId)
                    .input('UOM', mssql.NVarChar, data.UOM)
                    .input('isActive', mssql.Int, data.isActive)
                    .input('LastModifiedOn', mssql.DateTime, data.LastModifiedOn)
                    .input('LastModifiedBy', mssql.NVarChar, data.LastModifiedBy)
                    .execute('updateRecord')
            console.log('---',updateRecord)
        if (updateRecord.returnValue === 0) return res.status(200).json({
                'status': 'Record is successfully updated'
            })
        return res.status(404).json({'status': queryStatus[updateRecord.returnValue]})
    }catch (err) {
        console.log(err);
        return res.status(500).json({'status': err.message})
      }
}

module.exports = { insertRecords, getAllRecords, getRecordById, deletRecordById, updateRecord}