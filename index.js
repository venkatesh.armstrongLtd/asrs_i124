const http = require('http')
const app = require('./app')
const mssql = require('mssql')
const sqlConfig = require('./db/dbconfig')

process.on('uncaughtException', err => {
  console.log(`${err.name} : ${err.message}`);
  console.log('UNHANDLED EXCEPTION : Shutting down ..');
  process.exit(1);
});

const server = http.createServer(app);

const dbConnection = async () => {
    try {
        // make sure that any items are correctly URL encoded in the connection string
        const result = mssql.connect(sqlConfig)
        if (result) console.log('Database is connected successfully...')
        // console.dir(result)
    } catch (err) {
        console.log('DB CONNECTION ERROR: ', err)
    }
}
dbConnection()

const port = process.env.PORT || 5000;
server.listen(port, () => console.log('App is listening at port - ' + port));

process.on('unhandledRejection', err => {
  console.log(`${err.name} : ${err.message}`);
  console.log('UNHANDLED REJECTION : Shutting down ..');
  server.close(() => {
    process.exit(1);
  });
});
