const express = require('express')
const cors = require('cors')
const morgan = require('morgan')
const apiRouter = require('./Routes/apiRouter')

const app = express();

app.use(morgan('dev'));
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/api', apiRouter);

app.get('/', (req, res)=> res.send('home page'))

app.all('/*', (req, res, next) => {
  res.status(404).json('End point not Exist')
});

app.use((err, req, res, next) => {
  console.log(err);
  res.status(err.code || 500).json(err.message)
});

module.exports = app
