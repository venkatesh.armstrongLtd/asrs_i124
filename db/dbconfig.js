require('dotenv').config()

const  dbconfig = {
    user: process.env.DBUSERNAME,
    password:  process.env.DBPASSWORD,
    server: process.env.SERVERNAME,
    database: process.env.DBNAME,
    options: {
      trustedconnection:  true,
      trustServerCertificate: true,
      enableArithAbort:  true,
      instancename:  'SQLEXPRESS'  // SQL Server instance name
    },
    port: parseInt(process.env.DBSERVERPORT)
  }
  
  module.exports = dbconfig;